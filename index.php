<?php 

class TreeNode 
{
  function __construct($id, $title) 
  {
    $this->title = $title;
    $this->id = $id;   
    $this->children = [];     
  }

  public function getTemplate() {
    return "<li>{$this->title}</li>";
  }
}


class Tree {
  public $titles = [
    null => 'root'
  ];
  public $structure = [
    null => []
  ];    
  
  public function add($id, $parentId, $title) 
  { 
    $currentParent = & $this->structure[$parentId];
    if (!isset($currentParent)) {
      $currentParent = [];
    }

    $currentChild = & $this->structure[$id];
    if (!isset($currentChild)) {
      $currentChild = [];
    }
    
    $this->titles[$id] = $title;
    $this->structure[$parentId][] = $id;   
  }

  private function buildTree($nodeId) 
  {
    $nodeTitle = $this->titles[$nodeId];
    $node = new TreeNode($nodeId, $nodeTitle);

    $node->children = array_map(
      function($nodeId) {
        return $this->buildTree($nodeId);
      },
      $this->structure[$nodeId]
    );
    
    return $node;
  }

  function treeToString($node=null) 
  {   
    $result = "<ul>";           
    $result .= $node->getTemplate();
    $v = 0;    
    $result .= "<ul style='padding-left: 20px;'>";

    while ($v < count($node->children)) {
      $result .= $this->treeToString($node->children[$v]);
      $v += 1;
    }    
    $result .= "</ul></ul>";    
      
    return $result;
  }

  function renderTree() 
  {
    $tree = $this->buildTree(null);        
    return $this->treeToString($tree);
  }
}


$tree = new Tree();

$tree->add(1, null, 'первый');

$tree->add(2, 1, 'второй');

$tree->add(3, 1, 'третий');

$tree->add(4, 2, 'четвертый');

$tree->add(5, 2, 'пятый');

$tree->add(6, 7, 'шестой');

$tree->add(7, 1, 'седьмой');

$tree->add(8, 3, 'восьмой');

$tree->add(9, null, 'девятый');

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?= $tree->renderTree(); ?>
</body>
</html>